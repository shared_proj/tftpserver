/*
 * disc_b_cmd.h
 *
 *  Created on: Jan 16, 2017
 *      Author: DimaD
 */

#ifndef INCLUDE_BACKEND_DISC_B_CMD_H_
#define INCLUDE_BACKEND_DISC_B_CMD_H_

#include "command.h"

class disc_b_cmd: public command {
	short m_received_op;
	bool& m_run;
public:
	disc_b_cmd(concurrent_vector& op_vector,ConnectionHandler& connectionHandler, short received_op,bool& run);
	bool execute();
	void set_run(bool* run);
};

#endif /* INCLUDE_BACKEND_DISC_B_CMD_H_ */
