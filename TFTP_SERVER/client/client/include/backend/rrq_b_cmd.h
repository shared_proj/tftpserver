/*
 * rrq_b_cmd.h
 *
 *  Created on: Jan 16, 2017
 *      Author: DimaD
 */

#ifndef INCLUDE_BACKEND_RRQ_B_CMD_H_
#define INCLUDE_BACKEND_RRQ_B_CMD_H_

#include "command.h"

class rrq_b_cmd: public command {
	short m_received_op;
public:
	rrq_b_cmd(concurrent_vector& op_vector,ConnectionHandler& connectionHandler, short received_op);
	bool execute();
	bool got_error_msg();
};
#endif /* INCLUDE_BACKEND_RRQ_B_CMD_H_ */
