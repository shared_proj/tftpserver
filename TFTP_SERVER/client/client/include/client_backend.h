/*
 * client_backend.h
 *
 *  Created on: Jan 16, 2017
 *      Author: DimaD
 */

#ifndef INCLUDE_CLIENT_BACKEND_H_
#define INCLUDE_CLIENT_BACKEND_H_

#include <concurrent_vector.h>
#include <connectionHandler.h>
#include <stdlib.h>
#include <backend/logrq_b_cmd.h>
#include <backend/disc_b_cmd.h>
#include <backend/dirq_b_cmd.h>
#include <backend/delrq_b_cmd.h>
#include <backend/bcast_b_cmd.h>
#include <backend/wrq_b_cmd.h>
#include <backend/rrq_b_cmd.h>


class client_back_end {
	concurrent_vector& m_op_vector;
	ConnectionHandler& m_connectionHandler;
	bool& m_run;
public:
	client_back_end(concurrent_vector& op_vector, ConnectionHandler& connectionHandler, bool&  run );
	void run();
	command* str2cmd(short s_cmd, short reviced_op_code,bool& run);
};

#endif /* INCLUDE_CLIENT_BACKEND_H_ */
