
#ifndef SRC_CLI_COMMAND_H_
#define SRC_CLI_COMMAND_H_

#include "command.h"

using namespace std;

class cli_command: public command {

protected :
	string m_param ;

public:

	cli_command(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler, string param);
	virtual ~cli_command(){};
	bool execute ();
	void push_filename_ (string filename);
	virtual string get_packet() = 0;
	virtual short get_op_code() = 0;
};

#endif /* SRC_CLI_COMMAND_H_ */
