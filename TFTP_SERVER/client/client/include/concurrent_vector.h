/*
 * concurrent_vector.h
 *
 *  Created on: Jan 15, 2017
 *      Author: DimaD
 */

#ifndef INCLUDE_CONCURRENT_VECTOR_H_
#define INCLUDE_CONCURRENT_VECTOR_H_
using namespace std;

#include <boost/thread.hpp>
#include <deque>

class concurrent_vector{
	boost::mutex&  m_mutex;
	deque<short> m_deque ;
	string m_file_name = string("");

public:
	concurrent_vector(boost::mutex& mutex);
	void push_back(short item);
        short pop_front();
	bool not_empty ();
	void push_filename(string file_name);
 	string pop_filename();
};

#endif /* INCLUDE_CONCURRENT_VECTOR_H_ */


