/*
 * client_front_end.h
 *
 *  Created on: Jan 15, 2017
 *      Author: DimaD
 */

#ifndef INCLUDE_CLIENT_FRONT_END_H_
#define INCLUDE_CLIENT_FRONT_END_H_

#include <concurrent_vector.h>
#include <connectionHandler.h>
#include <cli/logrq_cmd.h>
#include <cli/disc_cmd.h>
#include <cli/dirq_cmd.h>
#include <cli/delrq_cmd.h>
#include <cli/wrq_cmd.h>
#include <cli/rrq_cmd.h>



#include <stdlib.h>
#include <command.h>

class client_front_end {
	concurrent_vector& m_op_vector;
	ConnectionHandler& m_connectionHandler;
	bool* m_run;
public:
	client_front_end(concurrent_vector& op_vector, ConnectionHandler& connectionHandler ,bool*);
	void run();
	command* str2cmd(string s_cmd,string params);
};

#endif /* INCLUDE_CLIENT_FRONT_END_H_ */
