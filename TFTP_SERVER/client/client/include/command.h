/*
 * command.h
 *
 *  Created on: Jan 15, 2017
 *      Author: DimaD
 */

#ifndef INCLUDE_COMMAND_H_
#define INCLUDE_COMMAND_H_

#include <concurrent_vector.h>
#include <connectionHandler.h>

using namespace std;

#define ACTION_SIZE 1
#define OP_CODE_BYTE_SIZE 2
#define ERROR_CODE_BYTE_SIZE 2
#define BLOCK_NUM_SIZE 2
#define PACKET_SIZE_SIZE 2
#define MAX_PAYLOAD_SIZE  512


#define RRQ_OP_CODE 1
#define WRQ_OP_CODE 2
#define DATA_OP_CODE 3
#define ACK_OP_CODE 4
#define ERROR_OP_CODE 5
#define DIRQ_OP_CODE 6
#define LOGRQ_OP_CODE 7
#define DELRQ_OP_CODE 8
#define BCAST_OP_CODE 9
#define DISC_OP_CODE 10

class command {

protected :
	concurrent_vector& m_ops_vector;
	ConnectionHandler& m_connectionHandler;

public:
	command(concurrent_vector& ops_vector, ConnectionHandler& connectionHandler);
	virtual ~command(){}
	static short bytesToShort(const char* bytesArr);
	static void shortToBytes(short num, char* bytesArr);
	virtual bool execute() = 0;
	virtual void set_run(bool* ){};
};

#endif /* INCLUDE_COMMAND_H_ */
