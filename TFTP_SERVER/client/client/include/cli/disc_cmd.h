/*
 * disc_cmd.h
 *
 *  Created on: Jan 14, 2017
 *      Author: DimaD
 */
#include <cli_command.h>

#ifndef SRC_CLI_DISCCMD_H_
#define SRC_CLI_DISCCMD_H_

class disc_cmd : public cli_command
{
	public:
	disc_cmd(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler, string param);
		
	string get_packet();
 	short get_op_code();
};

#endif /* SRC_CLI_DISCCMD_H_ */
