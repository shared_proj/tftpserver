/*
 * logrq_cmd.h
 *
 *  Created on: Jan 13, 2017
 *      Author: DimaD
 */
#include <cli_command.h>

#ifndef SRC_LOGRQ_CMD_H_
#define SRC_LOGRQ_CMD_H_

class logrq_cmd : public cli_command
{
	public:
		logrq_cmd(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler, string param);
		string get_packet();
		short get_op_code();
		string get_cmd_name();
};

#endif /* SRC_LOGRQ_CMD_H_ */
