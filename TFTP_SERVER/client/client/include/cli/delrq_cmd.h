/*
 * delrq_cmd.h
 *
 *  Created on: Jan 13, 2017
 *      Author: DimaD
 */
#include <cli_command.h>

#ifndef SRC_DELRQ_CMD_H_
#define SRC_DELRQ_CMD_H_

class delrq_cmd : public cli_command
{
	public:
		delrq_cmd(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler, string param);
		string get_packet();
		short get_op_code();
};

#endif /* SRC_DELRQ_CMD_H_ */
