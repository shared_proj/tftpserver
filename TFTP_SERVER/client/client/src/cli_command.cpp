#include <cli_command.h>

#ifndef SRC_CLI_COMMAND_CPP_
#define SRC_CLI_COMMAND_CPP_

cli_command::cli_command(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler, string param)
	:command(ops_vector,connectionHandler),m_param(param){}

bool cli_command::execute (){

	m_ops_vector.push_back(get_op_code());
	if ( DISC_OP_CODE == get_op_code() ||  DIRQ_OP_CODE == get_op_code() ){
		string packet = get_packet();
		if (false == m_connectionHandler.sendBytes( packet.c_str(), OP_CODE_BYTE_SIZE )) {
 	               	cout << "sendLine Failed ";
                	return false;
		}
        }else{
		if (false == m_connectionHandler.sendLine(get_packet().append("")) ) {
			cout << "sendLine Failed ";
			return false;
		}
	}
	return true;
}

void  cli_command::push_filename_ (string filename){
	m_ops_vector.push_filename(filename);
}
#endif /* SRC_CLI_COMMAND_CPP_ */
