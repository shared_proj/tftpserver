/*
 * bcast_b_cmd.cpp
 *
 *  Created on: Jan 16, 2017
 *      Author: DimaD
 */

#include <backend/bcast_b_cmd.h>

bcast_b_cmd::bcast_b_cmd(concurrent_vector& op_vector,ConnectionHandler& connectionHandler, short received_op)
	:command(op_vector,connectionHandler), m_received_op(received_op){}


bool bcast_b_cmd::execute(){

		string action;

		char action_num[ACTION_SIZE];
		m_connectionHandler.getBytes(action_num , ACTION_SIZE);
		switch ( action_num[0]) {
			case 1:
				action = string("add ");
				break;
			case 0: 
				action = string("del "); 
				break;
			default :
				cout << "BCAST wromg action "<< action_num[0];
				return false; 
		}

		string  file_name;
		if(false == m_connectionHandler.getLine(file_name)){
			cout << "getLine Failed ";
			return false;
		}
		cout << string("BCAST ") << action << file_name << endl;
		
		return true;
}
