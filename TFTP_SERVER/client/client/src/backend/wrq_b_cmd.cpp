/*
 * wrq_b_cmd.cpp
 *
 *  Created on: Jan 16, 2017
 *      Author: DimaD
 */

#include <backend/wrq_b_cmd.h>
#include <iostream>
#include <fstream>
#include <cstddef>

wrq_b_cmd::wrq_b_cmd(concurrent_vector& op_vector,ConnectionHandler& connectionHandler, short received_op)
	:command(op_vector,connectionHandler), m_received_op(received_op){}


bool wrq_b_cmd::execute()
{
	bool rc = false;
	char op_code_ch[OP_CODE_BYTE_SIZE];
	short op_code;
	size_t size = 0;
	short block_num;

	char block_num_ch [BLOCK_NUM_SIZE];
	string file = m_ops_vector.pop_filename();
        ifstream inFile;

	switch(m_received_op){

			case ACK_OP_CODE:
			{
				// get block num
				m_connectionHandler.getBytes(block_num_ch, BLOCK_NUM_SIZE);
				block_num = bytesToShort(block_num_ch);

				 cout << "ACK " << block_num << endl;
				inFile.open( file, ios::in|ios::binary);
				if  (inFile.is_open()){
					char* Data = new char[MAX_PAYLOAD_SIZE] ;
					short current_block=1;

					inFile.seekg(0, ios::end); 
					size = inFile.tellg();
//					cout << "Size of file: " << size;//TODO remove
					inFile.seekg(0, ios::beg);
	
					rc = true;
					char* packet = new char [ OP_CODE_BYTE_SIZE + BLOCK_NUM_SIZE+ PACKET_SIZE_SIZE + MAX_PAYLOAD_SIZE ];
					do{ 
						if (size < MAX_PAYLOAD_SIZE){
							delete packet;
							packet = new char[OP_CODE_BYTE_SIZE + BLOCK_NUM_SIZE + PACKET_SIZE_SIZE + size];
						}

						packet[0] = 0;
						packet[1] = DATA_OP_CODE;
						shortToBytes(size,packet+2);
						shortToBytes(current_block,packet+4);
						
						if (size >= MAX_PAYLOAD_SIZE){
							shortToBytes(MAX_PAYLOAD_SIZE,packet+2);
                                                        shortToBytes(current_block,packet+4);
							inFile.read(Data, MAX_PAYLOAD_SIZE);
							memcpy(packet+6, Data, MAX_PAYLOAD_SIZE);
						   	m_connectionHandler.sendBytes(packet, OP_CODE_BYTE_SIZE + BLOCK_NUM_SIZE + PACKET_SIZE_SIZE + MAX_PAYLOAD_SIZE);
                                                        size-=MAX_PAYLOAD_SIZE;
						}else{
							shortToBytes(size,packet+2);
                                                	shortToBytes(current_block,packet+4);
							inFile.read(Data, size);
							memcpy(packet+6, Data, size);
							m_connectionHandler.sendBytes(packet, OP_CODE_BYTE_SIZE + BLOCK_NUM_SIZE + PACKET_SIZE_SIZE + size);
							size=0;
						}						

						m_connectionHandler.getBytes(op_code_ch, OP_CODE_BYTE_SIZE);
                                                op_code = bytesToShort(op_code_ch); 
						switch (op_code){ 
							 case ACK_OP_CODE:
		                		                m_connectionHandler.getBytes(block_num_ch, BLOCK_NUM_SIZE);
        		                        		block_num = bytesToShort(block_num_ch);
								if (block_num != current_block){
									cout << "diffrent block current_block" <</* "I THINK : " */ current_block  << endl;	
								}
								cout << "ACK " << block_num << endl;
								current_block++;
								break;
							 case ERROR_OP_CODE:
								got_error_msg();
								inFile.close();
								return false;
							default :
								cout << "wrong OP code from server. OP = " << op_code << endl;	
								return false;
						}
	
					}while(size!=0);
					inFile.close();
				}
				break;
			}

			case ERROR_OP_CODE:
			{
				rc = false;
				got_error_msg();
				break;
			}

			default:
				rc = false;
				cout << "wrong OP code from server. OP = " << m_received_op << endl;
		}
		return rc;
}

bool wrq_b_cmd::got_error_msg()
{
	string err_msg;
	char error_code_ch[ERROR_CODE_BYTE_SIZE] = {};
	if (false == m_connectionHandler.getBytes(error_code_ch , ERROR_CODE_BYTE_SIZE))
	{
		cout << "getBytes Failed ";
		return false;
	}
	short error_code = bytesToShort(error_code_ch);
	cout << "Error " << error_code << endl;//<< " msg : " << err_msg << endl;

	if(false == m_connectionHandler.getLine(err_msg)){
		cout << "getLine Failed ";
		return false;
	}
	return false;
}

