/*
 * rrq_b_cmd.cpp
 *
 *  Created on: Jan 16, 2017
 *      Author: DimaD
 */

#include <backend/rrq_b_cmd.h>
#include <backend/bcast_b_cmd.h>

#include <iostream>
#include <fstream>
#include <cstddef>

rrq_b_cmd::rrq_b_cmd(concurrent_vector& op_vector,ConnectionHandler& connectionHandler, short received_op)
	:command(op_vector,connectionHandler), m_received_op(received_op){}


bool rrq_b_cmd::execute()
{
	bool rc = false;

	switch(m_received_op){

			case ACK_OP_CODE:
			{
                                char block_num_ch[BLOCK_NUM_SIZE];
                                m_connectionHandler.getBytes(block_num_ch, BLOCK_NUM_SIZE); 
				string file = m_ops_vector.pop_filename(); 
				short data_size = MAX_PAYLOAD_SIZE;
				ofstream  oFile;
				oFile.open( file, ios::out|ios::trunc);
				char data[MAX_PAYLOAD_SIZE];
				short block_num = 1;
				do 
				{	
					char op_code_ch[OP_CODE_BYTE_SIZE];
                                	m_connectionHandler.getBytes(op_code_ch, OP_CODE_BYTE_SIZE);
                                	short op_code = bytesToShort(op_code_ch); 
					
					switch (op_code)
					{

						case DATA_OP_CODE:
						{
							char packet_size_ch[PACKET_SIZE_SIZE];
		                                        m_connectionHandler.getBytes(packet_size_ch, PACKET_SIZE_SIZE);
                		                        data_size = bytesToShort(packet_size_ch); 
							
							m_connectionHandler.getBytes(block_num_ch, BLOCK_NUM_SIZE);
                                                        short packet_block_num = bytesToShort(block_num_ch);

							if (packet_block_num != block_num++){
								cout << " TODO send ERROR";
								return false; 
							}

							m_connectionHandler.getBytes(data, data_size);
							oFile.write(data,data_size);
							
							// send ACK
							op_code = ACK_OP_CODE;
							shortToBytes(op_code, op_code_ch);
							shortToBytes(packet_block_num, block_num_ch); 
						 	memcpy(data,op_code_ch,OP_CODE_BYTE_SIZE);
							memcpy(data+OP_CODE_BYTE_SIZE, block_num_ch ,PACKET_SIZE_SIZE);
							m_connectionHandler.sendBytes(data, OP_CODE_BYTE_SIZE + PACKET_SIZE_SIZE);

							break;
						}	       
						case BCAST_OP_CODE:
						{
							bcast_b_cmd bcast(m_ops_vector,m_connectionHandler, op_code);
							bcast.execute();
							break;	
						}
						default:
				                     rc = false;
                                		     cout << "wrong OP code from server. OP = " << m_received_op << endl;

					}
				}while(MAX_PAYLOAD_SIZE == data_size);
				cout  << "RRQ "<< file << " complete" << endl;
				oFile.close();
			}

			case ERROR_OP_CODE:
			{
				rc = false;
				got_error_msg();
				break;
			}

			default:
				rc = false;
				cout << "wrong OP code from server. OP = " << m_received_op << endl;
		}
		return rc;
}

bool rrq_b_cmd::got_error_msg()
{
	string err_msg;
	char error_code_ch[ERROR_CODE_BYTE_SIZE] = {};
	if (false == m_connectionHandler.getBytes(error_code_ch , ERROR_CODE_BYTE_SIZE))
	{
		cout << "getBytes Failed ";
		return false;
	}
	short error_code = bytesToShort(error_code_ch);
	cout << "Error " << error_code<< endl ;//<< " msg : " << err_msg << endl;

	if(false == m_connectionHandler.getLine(err_msg)){
		cout << "getLine Failed ";
		return false;
	}
	return false;
}

