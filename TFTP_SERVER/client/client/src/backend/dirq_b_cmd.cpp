/*
 * dirq_b_cmd.cpp
 *
 *  Created on: Jan 16, 2017
 *      Author: DimaD
 */

#include <backend/dirq_b_cmd.h>
#include <backend/bcast_b_cmd.h>

dirq_b_cmd::dirq_b_cmd(concurrent_vector& op_vector,ConnectionHandler& connectionHandler, short received_op)
	:command(op_vector,connectionHandler), m_received_op(received_op){}


bool dirq_b_cmd::execute(){
	bool rc = false;
	switch(m_received_op){

			case DATA_OP_CODE:
			{
				short last_block_num = 0;
				short packet_size = 512;
				rc = true;
				string list("");
				short op_code = m_received_op;

				do{
                                        switch (op_code)
                                        {

					        case BCAST_OP_CODE:
                                                {
                                                        bcast_b_cmd bcast(m_ops_vector,m_connectionHandler, op_code);
                                                        bcast.execute();
                                                        break;
                                                 }
						 case DATA_OP_CODE:
						 {
							// get data size
							char packet_size_ch[PACKET_SIZE_SIZE];
							m_connectionHandler.getBytes(packet_size_ch, PACKET_SIZE_SIZE);
							packet_size = bytesToShort(packet_size_ch);
						
							// get block num
        	        	       	        	char block_num_ch[BLOCK_NUM_SIZE];
              	                			m_connectionHandler.getBytes(block_num_ch, BLOCK_NUM_SIZE);
		                        	        short block_num = bytesToShort(block_num_ch);
	
							if (++last_block_num != block_num){
								// send ERROR 
								return false ;
							}
							char* data  = new char[packet_size];
							m_connectionHandler.getBytes(data, packet_size);

							for(int i=0 ; i < packet_size; i++)
							{
								if (data[i]==0){
									data[i] = '\n';
								}
							} 					
							list.append(string(data)); 
							delete data;
							break;
						}
						default:
							cout << "wrong OP code from server. OP = " << op_code << endl;
							break;

					}
					if (packet_size==512){	
						char op_code_ch[OP_CODE_BYTE_SIZE];
                                        	m_connectionHandler.getBytes(op_code_ch, OP_CODE_BYTE_SIZE);
                                        	op_code = bytesToShort(op_code_ch);
					}
				}while(packet_size==512);
				cout << list;
				break;
			}

			case ERROR_OP_CODE:
			{
				rc = false;
				string err_msg;
				char error_code_ch[ERROR_CODE_BYTE_SIZE] = {};
				if (false == m_connectionHandler.getBytes(error_code_ch , ERROR_CODE_BYTE_SIZE))
				{
					cout << "getBytes Failed ";
					return false;
				}
				short error_code = bytesToShort(error_code_ch);
				cout << "Error " << error_code << endl;//<< " msg : " << err_msg << endl;

				if(false == m_connectionHandler.getLine(err_msg)){
					cout << "getLine Failed ";
					return false;
				}
				break;
			}
			default:
				rc = false;
				cout << "wrong OP code from server. OP = " << m_received_op << endl;
		}
		return rc;
}
