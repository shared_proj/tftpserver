/*
 * disc_b_cmd.cpp
 *
 *  Created on: Jan 16, 2017
 *      Author: DimaD
 */

#include <backend/disc_b_cmd.h>

disc_b_cmd::disc_b_cmd(concurrent_vector& op_vector,ConnectionHandler& connectionHandler, short received_op, bool& run )
	:command(op_vector,connectionHandler), m_received_op(received_op), m_run(run){}


bool disc_b_cmd::execute(){
	bool rc = false;
	switch(m_received_op){

			case ACK_OP_CODE:
			{
				rc = true;
				char block_num[BLOCK_NUM_SIZE];
				m_connectionHandler.getBytes(block_num , OP_CODE_BYTE_SIZE);
				cout << "ACK " << bytesToShort(block_num) << endl;
				m_connectionHandler.close();
			break;
			}

			case ERROR_OP_CODE:
			{
				rc = false;
				string err_msg;
				char error_code_ch[ERROR_CODE_BYTE_SIZE] = {};
				if (false == m_connectionHandler.getBytes(error_code_ch , ERROR_CODE_BYTE_SIZE))
				{
					cout << "getBytes Failed ";
					return false;
				}
				short error_code = bytesToShort(error_code_ch);
				cout << "Error " << error_code << endl;//<< " msg : " << err_msg << endl;

				if(false == m_connectionHandler.getLine(err_msg)){
					cout << "getLine Failed ";
					return false;
				}
				m_run = true;
				break;
			}
			default:
				rc = false;
				cout << "wrong OP code from server. OP = " << m_received_op << endl;
		}
		return rc;
}

void disc_b_cmd::set_run(bool* run){
	m_run = run;
}

