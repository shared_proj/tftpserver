/*
 * dirq_cmd.cpp
 *
 *  Created on: Jan 14, 2017
 *      Author: DimaD
 */

#include "cli/dirq_cmd.h"

dirq_cmd::dirq_cmd(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler, string param)
        :cli_command(ops_vector, connectionHandler, param){}



string dirq_cmd::get_packet(){
        char op[OP_CODE_BYTE_SIZE]={};
        shortToBytes(DIRQ_OP_CODE, op);

        string sent_packet(op, OP_CODE_BYTE_SIZE);
        return sent_packet;
 }

 short dirq_cmd::get_op_code(){
         return DIRQ_OP_CODE;
 }



