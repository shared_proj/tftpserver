/*
 * rrq_cmd.cpp
 *
 *  Created on: Jan 13, 2017
 *      Author: DimaD
 */

#include <cli/rrq_cmd.h>
#include <cstring>

rrq_cmd::rrq_cmd(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler, string param)
	:cli_command(ops_vector, connectionHandler, param){}


 string rrq_cmd::get_packet(){
	char op[OP_CODE_BYTE_SIZE]={};
	shortToBytes(RRQ_OP_CODE, op);

	string sent_packet(op, OP_CODE_BYTE_SIZE);
	sent_packet.append(m_param);
	push_filename_(m_param);
	return sent_packet;
 }

 short rrq_cmd::get_op_code(){
	 return RRQ_OP_CODE;
 }
