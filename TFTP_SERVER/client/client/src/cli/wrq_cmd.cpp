/*
 * wrq_cmd.cpp
 *
 *  Created on: Jan 13, 2017
 *      Author: DimaD
 */

#include <cli/wrq_cmd.h>
#include <cstring>

wrq_cmd::wrq_cmd(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler, string param)
	:cli_command(ops_vector, connectionHandler, param){}


 string wrq_cmd::get_packet(){
	char op[OP_CODE_BYTE_SIZE]={};
	shortToBytes(WRQ_OP_CODE, op);

	string sent_packet(op, OP_CODE_BYTE_SIZE);
	sent_packet.append(m_param);
	push_filename_(m_param);
	return sent_packet;
 }

 short wrq_cmd::get_op_code(){
	 return WRQ_OP_CODE;
 }
