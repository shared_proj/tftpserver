/*
 * disccmd.cpp
 *
 *  Created on: Jan 14, 2017
 *      Author: DimaD
 */

#include "cli/disc_cmd.h"

disc_cmd::disc_cmd(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler, string param)
        :cli_command(ops_vector, connectionHandler, param){}



string disc_cmd::get_packet(){
        char op[OP_CODE_BYTE_SIZE]={};
        shortToBytes(DISC_OP_CODE, op);

        string sent_packet(op, OP_CODE_BYTE_SIZE);
        return sent_packet;
 }

 short disc_cmd::get_op_code(){
         return DISC_OP_CODE;
 }



/*
	bool rc = false;
	cout << "Running Disconect CMD ..." << endl;
	char op[OP_CODE_BYTE_SIZE]={};

	shortToBytes(DISC_OP_CODE, op);

	string sent_packet(op, OP_CODE_BYTE_SIZE);
	if (false == connectionHandler->sendLine(sent_packet)) {
		cout << "sendLine Failed ";
		return false;
	}

	char op_code_ch[OP_CODE_BYTE_SIZE] = {};
	if (false == connectionHandler->getBytes(op_code_ch , OP_CODE_BYTE_SIZE)){
		cout << "getBytes Failed ";
		return false;
	}
	short op_code = bytesToShort(op_code_ch);

	switch(op_code){
		case ACK_OP_CODE:
		{
			rc = true;
			char junk[BLOCK_NUM_SIZE];
			junk[0]=0;
			connectionHandler->getBytes(junk , OP_CODE_BYTE_SIZE); //just to clean buffer

			connectionHandler->close();
			cout << "Successfully dis-connected from server";//TODO: remove
			break;
		}

	default:
			rc = false;
			cout << "wrong OP code from server. OP = " << op_code << endl;
	}
	return rc;

}*/
