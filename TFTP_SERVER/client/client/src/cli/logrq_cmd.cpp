/*
 * logrq_cmd.cpp
 *
 *  Created on: Jan 13, 2017
 *      Author: DimaD
 */

#include <cli/logrq_cmd.h>
#include <cstring>

logrq_cmd::logrq_cmd(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler, string param)
	:cli_command(ops_vector, connectionHandler, param){}


 string logrq_cmd::get_packet(){
	char op[OP_CODE_BYTE_SIZE]={};
	shortToBytes(LOGRQ_OP_CODE, op);

	string sent_packet(op, OP_CODE_BYTE_SIZE);
	sent_packet.append(m_param);
	return sent_packet;
 }

 short logrq_cmd::get_op_code(){
	 return LOGRQ_OP_CODE;
 }

/*
bool logrq_cmd::execute(ConnectionHandler* connectionHandler) {
	bool rc = false;
	cout << " logging in as " << m_param << endl;

	char op_code_ch[OP_CODE_BYTE_SIZE] = {};
	if (false == connectionHandler->getBytes(op_code_ch , OP_CODE_BYTE_SIZE)){
		cout << "getBytes Failed ";
		return false;
	}
	short op_code = bytesToShort(op_code_ch);

	switch(op_code){
		case ACK_OP_CODE:
		{
			rc = true;
			char junk[BLOCK_NUM_SIZE];
			junk[0]=0;
			connectionHandler->getBytes(junk , OP_CODE_BYTE_SIZE); //just to clean buffer
			cout << "Successfully connected to server";//TODO: remove
			break;
		}
		case ERROR_OP_CODE:
		{
			rc = false;
			string err_msg;
			char error_code_ch[ERROR_CODE_BYTE_SIZE] = {};
			if (false == connectionHandler->getBytes(error_code_ch , ERROR_CODE_BYTE_SIZE))
			{
				cout << "getBytes Failed ";
				return false;
			}
			short error_code = bytesToShort(error_code_ch);

			if(false == connectionHandler->getLine(err_msg)){
				cout << "getLine Failed ";
				return false;
			}
			cout << "ErrorCode is " << error_code << " msg : " << err_msg;
			break;
		}
		default:
			rc = false;
			cout << "wrong OP code from server. OP = " << op_code << endl;
	}
	return rc;
}*/
