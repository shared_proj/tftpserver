/*
 * delrq_cmd.cpp
 *
 *  Created on: Jan 13, 2017
 *      Author: DimaD
 */

#include <cli/delrq_cmd.h>
#include <cstring>

delrq_cmd::delrq_cmd(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler, string param)
	:cli_command(ops_vector, connectionHandler, param){}


 string delrq_cmd::get_packet(){
	char op[OP_CODE_BYTE_SIZE]={};
	shortToBytes(DELRQ_OP_CODE, op);

	string sent_packet(op, OP_CODE_BYTE_SIZE);
	sent_packet.append(m_param);
	return sent_packet;
 }

 short delrq_cmd::get_op_code(){
	 return DELRQ_OP_CODE;
 }
