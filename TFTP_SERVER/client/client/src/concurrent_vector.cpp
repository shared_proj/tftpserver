/*
 * concurrent_vector.cpp
 *
 *  Created on: Jan 15, 2017
 *      Author: DimaD
 */

#include "concurrent_vector.h"

concurrent_vector::concurrent_vector(boost::mutex& mutex):m_mutex(mutex),m_deque(deque<short>(0)) {}

void concurrent_vector::push_back(short op){
	  boost::mutex::scoped_lock lock(m_mutex);
	  m_deque.push_back(op);
}

short concurrent_vector::pop_front(){
	short op = 0;
	boost::mutex::scoped_lock lock(m_mutex);
	if (m_deque.size() > 0){
	  op = m_deque.front();
	  m_deque.pop_front();
	}
	return op;
}

bool concurrent_vector::not_empty (){
 	boost::mutex::scoped_lock lock(m_mutex);
	return (m_deque.size() !=  0);
}

void concurrent_vector::push_filename(string file_name){
	boost::mutex::scoped_lock lock(m_mutex);
	m_file_name = file_name;
}
string concurrent_vector::pop_filename(){
	boost::mutex::scoped_lock lock(m_mutex);
	return m_file_name;
}
