/*
 * client_front_end.cpp
 *
 *  Created on: Jan 15, 2017
 *      Author: DimaD
 */

#include <client_front_end.h>

client_front_end::client_front_end(concurrent_vector& op_vector,ConnectionHandler& connectionHandler,bool* run)
	:m_op_vector(op_vector),m_connectionHandler(connectionHandler),m_run(run) {}

void client_front_end::run(){
   	command* cmd = NULL;
	while (m_connectionHandler.is_open()) try{

		string line;

		while(m_op_vector.not_empty() );
		
		if( *m_run){
		getline(cin, line);

		if(0 != line.compare("")){
			 string op_code  = line.substr(0, line.find(string(" ")));
			 string params ="";

			 if (op_code.size() < line.size()){
				 params = line.substr(op_code.size()+1, line.size() - line.find(string(" ")));
			 }

			 cmd = str2cmd(op_code, params);
			 if (params == "DISC"){
				*m_run = false;
			 }
			 if ( (NULL != cmd) && false == cmd->execute() ) {
				 cout << "cmd execution failed";
			 }
			}
		}
		boost::this_thread::interruption_point();

	}catch(...){
		return;
	} 
	return;
}

command* client_front_end::str2cmd(string s_cmd,string params){
	if ("LOGRQ" == s_cmd)
		return new logrq_cmd(m_op_vector, m_connectionHandler, params);
	
	if ("DISC" == s_cmd)
		return new disc_cmd(m_op_vector, m_connectionHandler, params);
	
	if ("DIRQ" == s_cmd)
                return new dirq_cmd(m_op_vector, m_connectionHandler, params);

        if ("DELRQ" == s_cmd)
                return new delrq_cmd(m_op_vector, m_connectionHandler, params);

        if ("WRQ" == s_cmd)
                return new wrq_cmd(m_op_vector, m_connectionHandler, params);
        if ("RRQ" == s_cmd)
                return new rrq_cmd(m_op_vector, m_connectionHandler, params);

	return NULL;
}
