/*
 * command.cpp
 *
 *  Created on: Jan 15, 2017
 *      Author: DimaD
 */

#include "command.h"

command::command(concurrent_vector& ops_vector,ConnectionHandler& connectionHandler)
	:m_ops_vector(ops_vector),m_connectionHandler(connectionHandler) {}

short command::bytesToShort(const char* bytesArr)
{
    short result = (short)((bytesArr[0] & 0xff) << 8);
    result += (short)(bytesArr[1] & 0xff);
    return result;
}

void command::shortToBytes(short num, char* bytesArr)
{
    bytesArr[0] = ((num >> 8) & 0xFF);
    bytesArr[1] = (num & 0xFF);
}
