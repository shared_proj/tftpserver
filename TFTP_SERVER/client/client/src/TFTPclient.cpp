#include <iostream>
#include <stdlib.h>
#include <connectionHandler.h>
#include <concurrent_vector.h>
#include <client_front_end.h>
#include <client_backend.h>

using namespace std;

int main (int argc, char *argv[]) {
    if (argc < 3) {
    	cerr << "Usage: " << argv[0] << " host port" << endl << endl;
        return -1;
    }
    string host = argv[1];
    short port = atoi(argv[2]);
    bool run = true;	
    boost::mutex socket_mutex;
    boost::mutex op_vector_mutex;
    ConnectionHandler connectionHandler(host, port, socket_mutex);
    concurrent_vector ops_vector(op_vector_mutex);

    if (!connectionHandler.connect()) {
        cerr << "Cannot connect to " << host << ":" << port << endl;
        return 1;
    }

    client_front_end front_end(ops_vector, connectionHandler, &run);
    client_back_end back_end(ops_vector, connectionHandler, run);

    boost::thread front_thread(&client_front_end::run, &front_end);
    //boost::thread back_thread(&client_back_end::run, &back_end);
    back_end.run();
    front_thread.interrupt();			
//front_thread.join();
    //back_thread.join();
    return 0;
}
