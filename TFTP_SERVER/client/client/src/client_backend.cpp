/*
 * client_backend.cpp
 *
 *  Created on: Jan 16, 2017
 *      Author: DimaD
 */

#include "client_backend.h"

client_back_end::client_back_end(concurrent_vector& op_vector, ConnectionHandler& connectionHandler, bool& run )
	:m_op_vector(op_vector),m_connectionHandler(connectionHandler),m_run(run) {}

void client_back_end::run(){
	while (m_connectionHandler.is_open()) {
		char op_code_ch[OP_CODE_BYTE_SIZE] = {};
		if (false == m_connectionHandler.getBytes(op_code_ch , OP_CODE_BYTE_SIZE)){
			cout << "getBytes Failed ";
		}
		short received_op_code = command::bytesToShort(op_code_ch);
		short last_cmd=0;
		if( BCAST_OP_CODE != received_op_code){
			 last_cmd = m_op_vector.pop_front();
		}else{
			last_cmd = BCAST_OP_CODE;
		}
		command* back_end_cmd = str2cmd(last_cmd, received_op_code, m_run);
		back_end_cmd->execute();
	}
}

command* client_back_end::str2cmd(short cmd ,short received_op_code, bool& run){
	if (LOGRQ_OP_CODE == cmd)
		return new logrq_b_cmd(m_op_vector, m_connectionHandler, received_op_code);

	if (DISC_OP_CODE == cmd)
               return new disc_b_cmd(m_op_vector, m_connectionHandler, received_op_code, run);

        if (DIRQ_OP_CODE == cmd)
               return new dirq_b_cmd(m_op_vector, m_connectionHandler, received_op_code);

        if (DELRQ_OP_CODE == cmd)
               return new delrq_b_cmd(m_op_vector, m_connectionHandler, received_op_code);

        if (BCAST_OP_CODE == cmd)
               return new bcast_b_cmd(m_op_vector, m_connectionHandler, received_op_code);

        if (WRQ_OP_CODE == cmd)
               return new wrq_b_cmd(m_op_vector, m_connectionHandler, received_op_code);

	if (RRQ_OP_CODE == cmd)
               return new rrq_b_cmd(m_op_vector, m_connectionHandler, received_op_code);

	return NULL;
}
