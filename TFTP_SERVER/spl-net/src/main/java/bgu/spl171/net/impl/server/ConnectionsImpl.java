package bgu.spl171.net.impl.server;
import bgu.spl171.net.api.bidi.Connections;
import bgu.spl171.net.impl.msg.Message;
import bgu.spl171.net.srv.ConnectionHandler;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class ConnectionsImpl<T> implements Connections<T>{

	private ConcurrentHashMap<Integer, ConnectionHandler<T>> connected_clients = new ConcurrentHashMap<Integer, ConnectionHandler<T>>();
	private ConcurrentHashMap<String, Integer> logged_in_clients = new ConcurrentHashMap<String, Integer>();
	
	/**
	 * Default constructor
	 */
	public ConnectionsImpl() {}
	
	/**
	 * Constructor
	 */
	public ConnectionsImpl(ConcurrentHashMap<Integer, ConnectionHandler<T>> con, ConcurrentHashMap<String, Integer> log)
	{
		connected_clients = con;
		logged_in_clients = log;
	}

	/**
	 * Sends a message T to client represented by the given connectionId
	 */
	@Override
	public boolean send(int connectionId, T msg) {
		if(connected_clients.containsKey(connectionId)){
			connected_clients.get(connectionId).send(msg);
			return true;
		}
		return false;
	}

	/**
	 * Sends a message T to all active  clients. 
	 * This includes clients that has not yet completed log-in by the extended TFTP
	 * protocol.
	 */
	@Override
	public void broadcast(T msg) {
		for(int i = Number.ZERO; i < connected_clients.size(); i++){
			connected_clients.get(i).send(msg);
		}
	}

	/**
	 * Removes active client connectionId from map.
	 */
	@Override
	public void disconnect(int connectionId) {
		if(connected_clients.containsKey(connectionId)){
			connected_clients.remove(connectionId);
		}		
	}
		
	//Getters
	/**
	 * @return ConcurrentHashMap<String, Integer> logged_in_clients
	 */
	public ConcurrentHashMap<String, Integer> get_logged_in_clients()
	{
		return logged_in_clients;
	}
	
	/**
	 * @return ConcurrentHashMap<Integer, ConnectionHandler<Message>> connected_clients
	 */
	public ConcurrentHashMap<Integer, ConnectionHandler<T>> get_connected_clients()
	{
		return connected_clients;
	}
	
	//Setters
	/**
	 * Adds a client to connected_clients
	 * @param conid is int
	 * @param handle is ConnectionHandler<T> 
	 */
	public void addconnectedclient(int conid, ConnectionHandler<T> handle)
	{
		connected_clients.put(conid, handle);
	}
	
	/**
	 * Removes a client connected_clients
	 * @param conid is int
	 */
	public void removeconnectedclient(int conid)
	{
		connected_clients.remove(conid);
	}
	
	/**
	 * Adds a client to logged_in_clients
	 * @param username is String
	 * @param conid is int
	 */
	public void addusername(String username, int conid)
	{
		logged_in_clients.put(username, conid);
	}
	
	/**
	 * Removes a client logged_in_clients
	 * @param username is String
	 */
	public void removeusername(String username)
	{
		logged_in_clients.remove(username);
	}

}
