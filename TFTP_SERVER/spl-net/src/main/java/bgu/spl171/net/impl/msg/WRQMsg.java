package bgu.spl171.net.impl.msg;

import bgu.spl171.net.impl.server.Packet;
import bgu.spl171.net.impl.server.ReadDataManager;
import bgu.spl171.net.api.bidi.Connections;
import bgu.spl171.net.impl.server.EncodeDecodeFunctions;
import bgu.spl171.net.impl.server.FilesHandler;
import bgu.spl171.net.impl.server.LoggedInClient;
import bgu.spl171.net.srv.BaseServer;


public class WRQMsg extends Message {

	private String fileName;
	private ReadDataManager read;
	/**
	 * Constructor
	 */
	public WRQMsg(ReadDataManager read)
	{
		opcode = Packet.WRQ;
		this.fileName = null;
		m_bytes = new byte[518];
		this.read=read;
	}
	
	/**
	 * Decodes the WRQ message
	 */
	@Override
	public Message decodeNextByte(byte newByte)
	{
		Message msg = null;
        if(newByte != 0) {
            m_bytes[len++] = newByte;
            System.out.println((char)newByte);
        }else{
        	fileName = encdec.decodeString(m_bytes);
            msg = this;
        }
        return msg;
	}
	
	/**
	 * Encodes the WRQ message
	 */
    public Message execute(Object... objs)
    {
            //does the file exist? if no - ack, if yes - error
            Connections<?> cons = (Connections<?>) objs[0];         
            LoggedInClient is_logged_in = (LoggedInClient)objs[1];
            Integer connid = (Integer) objs[2];
            for (String str : FilesHandler.files.keySet())
            {
                    if(this.fileName.equals(str)){
                            return new ERRORMsg((short)1);
                    }
            }       
            this.read.resetAll();
			read.SetFileName(this.fileName);
			//read.SetRWState(true);
            return new ACKMsg();
    }
    public String getFilename()
    {
    	return this.fileName;
    }

}
