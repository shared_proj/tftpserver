package bgu.spl171.net.impl.server;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;

public class FilesHandler {

	public static String path;
    public static ConcurrentHashMap<String, Integer> files;
    
    public FilesHandler(String path, ConcurrentHashMap<String, Integer> files)
    {
    	this.path = path;
    	this.files = files;
    }
    
    public void updateServerFiles()
    {
    	path += File.separator+"Files";
    	File folder = new File(path);
    	File[] listOfFiles = folder.listFiles();
    	for (int i = 0; i < listOfFiles.length; i++) {
    	      if (listOfFiles[i].isFile()) {
    	    	  System.out.println("File " + listOfFiles[i].getName());
    	    	  files.put(listOfFiles[i].getName(), FileState.NO_RW);
    	      }
    	 }
    }
    
    public static long getFreeSpace()
    {
    	return new File(path).getFreeSpace();
    }
}