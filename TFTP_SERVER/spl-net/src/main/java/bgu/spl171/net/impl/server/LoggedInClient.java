package bgu.spl171.net.impl.server;

/**
 * This class represents a user name client and if he logged in 
 */
public class LoggedInClient {

	private Boolean is_logged_in = false;
	private String username = "";
	
	public LoggedInClient(Boolean value)
	{
		is_logged_in = value;
	}
		
	public Boolean getis_logged_in()
	{
		return is_logged_in;
	}
	
	public void setis_logged_in(Boolean value)
	{
		is_logged_in = value;
	}
	
	public String getusername()
	{
		return username;
	}
	
	public void setusername(String str)
	{
		username = str;
	}
}
