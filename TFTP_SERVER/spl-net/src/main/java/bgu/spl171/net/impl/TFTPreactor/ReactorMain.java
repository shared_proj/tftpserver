package bgu.spl171.net.impl.TFTPreactor;

import bgu.spl171.net.impl.server.BidiMessagingProtocolImpl;
import bgu.spl171.net.impl.server.MessageEncoderDecoderImpl;
import bgu.spl171.net.impl.server.Number;
import bgu.spl171.net.srv.Server;

public class ReactorMain {

	 public static void main(String[] args) {
		 
		 Server.reactor(
	             Runtime.getRuntime().availableProcessors(),
	             Number.PORT, 
	             () ->  new BidiMessagingProtocolImpl(), //protocol factory
	             () ->  new MessageEncoderDecoderImpl() //message encoder decoder factory
	     ).serve();
	 }
}
