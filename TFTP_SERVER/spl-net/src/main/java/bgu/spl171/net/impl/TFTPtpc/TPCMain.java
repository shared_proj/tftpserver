package bgu.spl171.net.impl.TFTPtpc;

import bgu.spl171.net.impl.server.BidiMessagingProtocolImpl;
import bgu.spl171.net.impl.server.MessageEncoderDecoderImpl;
import bgu.spl171.net.impl.server.Number;
import bgu.spl171.net.srv.Server;

public class TPCMain {
	
	 public static void main(String[] args) {

        Server.threadPerClient(
        		Number.PORT,
        		() ->  new BidiMessagingProtocolImpl(), //protocol factory
        		() ->  new MessageEncoderDecoderImpl()
        		).serve();	     
	    }
}
