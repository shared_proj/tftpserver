package bgu.spl171.net.impl.server;

public class FileState {

	public static final int NO_RW = 0;
	public static final int IN_READ = 1;
	public static final int IN_WRITE = 2;
	
}
