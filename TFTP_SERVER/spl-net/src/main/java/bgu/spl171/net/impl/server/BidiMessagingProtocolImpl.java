package bgu.spl171.net.impl.server;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import bgu.spl171.net.api.bidi.BidiMessagingProtocol;
import bgu.spl171.net.api.bidi.Connections;
import bgu.spl171.net.impl.msg.ACKMsg;
import bgu.spl171.net.impl.msg.BCASTMsg;
import bgu.spl171.net.impl.msg.DATAMsg;
import bgu.spl171.net.impl.msg.DELRQMsg;
import bgu.spl171.net.impl.msg.DISCMsg;
import bgu.spl171.net.impl.msg.ERRORMsg;
import bgu.spl171.net.impl.msg.LOGRQMsg;
import bgu.spl171.net.impl.msg.Message;

public class BidiMessagingProtocolImpl implements BidiMessagingProtocol<Message>{

	private int connId = Number.ZERO;
	private Connections<Message> connections=null; 
	private boolean shouldterminate = false;
	private LinkedList<Byte> BytesToFile;

	private LoggedInClient is_logged_in = new LoggedInClient(false);
	
	/**
	 * initiate the protocol with the active connections structure of 
	 * the server and saves the owner client’s connection id.
	 */
	@Override
	public void start(int connectionId, Connections<Message> connections) {
		this.connId = connectionId;
		this.connections = connections;		
	}

	/**
	 * As in MessagingProtocol, processes a given message.    
	 * Unlike MessagingProtocol, responses are sent via the connections
	 * object send function.
	 */
	@Override
	public void process(Message message) {
		Message response = new ERRORMsg(Number.ZERO);
		short oc = message.getOpcode();
		switch (oc)
		{
		case Packet.DIRQ:
			if(is_client_connected() && is_client_logged_in())
				response = message.execute();
			break;
		case Packet.LOGRQ:
			if(is_client_connected())
				response = message.execute(connections, is_logged_in, new Integer(connId));
			break;
		case Packet.DELRQ:
			if(is_client_connected() && is_client_logged_in())
				response = message.execute();
			break;
		case Packet.DISC:
			if(is_client_connected() && is_client_logged_in())
				response = message.execute(connections, is_logged_in, new Integer(connId));
			break;
		case Packet.DATA:
			if(is_client_connected() && is_client_logged_in())
				response = message.execute(connections, is_logged_in, new Integer(connId));
			break;
		case Packet.ACK:
			response = message;
			break;
		case Packet.WRQ:
			if(is_client_connected() && is_client_logged_in())
				response = message.execute(connections, is_logged_in, new Integer(connId));
			break;	
		case 0://readmanager
			response = message.execute(connections, new Integer(connId));
			break;
			
		case Packet.RRQ:
			if(is_client_connected() && is_client_logged_in())
				response = message.execute(connections, is_logged_in, new Integer(connId));
			break;	
			
		default: 
			System.out.println("Wrong process opcode " + oc);
			break;
		}
		connections.send(connId, response);
		if(oc == Packet.DELRQ && response.getClass() != ERRORMsg.class)
		{
			connections.broadcast(new BCASTMsg(((DELRQMsg)message).getfilename(),(byte)Number.ZERO));
		}
	}
		
	/**
	 * returns if the connection is terminated
	 */
	@Override
	public boolean shouldTerminate() {
		return shouldterminate;
	}

	public boolean is_client_logged_in()
	{
		if(((ConnectionsImpl<?>)connections).get_logged_in_clients().containsKey(is_logged_in.getusername()))
			return true;
		return false;
	}
	
	public boolean is_client_connected()
	{
		if(((ConnectionsImpl<?>)connections).get_connected_clients().containsKey(connId))
			return true;
		return false;
	}
	
	public int getconnId()
	{
		return connId;
	}
	
	public void dataHandle(Message msg)
	{
		byte[] msgbytes = msg.encode();
		for (int i = Number.ZERO; i < msgbytes.length; i++)
			BytesToFile.add(msgbytes[i]);
		
		if(msgbytes.length < Number.PACKET_SIZE)
		{
			byte[] exactdata = new byte[BytesToFile.size()];
			for (int i = Number.ZERO; i < BytesToFile.size(); i++)
				exactdata[i] = BytesToFile.removeFirst();
			try {
				FileOutputStream out = new FileOutputStream("name from RRQ");
				out.write(exactdata);	
				out.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			 
		}

	}
}
