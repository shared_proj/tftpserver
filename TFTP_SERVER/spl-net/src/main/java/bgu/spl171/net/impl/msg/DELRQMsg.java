package bgu.spl171.net.impl.msg;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import bgu.spl171.net.impl.server.FilesHandler;
import bgu.spl171.net.impl.server.Number;
import bgu.spl171.net.impl.server.Packet;
import bgu.spl171.net.srv.BaseServer;

public class DELRQMsg extends Message {

	private String filename;
	
	/**
	 * Default constructor
	 */
    public DELRQMsg()
    {    
    	m_bytes = new byte[Number.MAX_LIMIT];
    	filename = "";
    	opcode = Packet.DELRQ;
    }
    
    /**
	 * Decodes the DELRQ message
	 */
	@Override
	public Message decodeNextByte(byte newByte)
	{
		Message msg = null;
        if(newByte != Number.ZERO) {
            m_bytes[len++] = newByte;
        }else{
        	filename = encdec.decodeString(m_bytes);
            msg = this;
        }
        return msg;
	}
	
	@Override
    public Message execute(Object... objs) {
		Message response = new ERRORMsg(Number.ONE);
		if (FilesHandler.files.containsKey(filename))
		{
			if (FilesHandler.files.get(filename) == Number.ZERO)
			{
				String pathtofile = FilesHandler.path + "/" + filename;
				File file = new File(pathtofile);
	    		if(file.delete())
	    		{
	    			System.out.println(file.getName() + " is deleted!");
	    			FilesHandler.files.remove(filename);
	    			response = null;
	    			response = new ACKMsg();
	    		}
	    		else
	    		{
	    			System.out.println("Delete operation is failed.");
	    			response = null;
	    			response = new ERRORMsg(Number.ONE);
	    		}
			}
		}
		return response;
	}
	
	public String getfilename()
	{
		return filename;
	}
}
