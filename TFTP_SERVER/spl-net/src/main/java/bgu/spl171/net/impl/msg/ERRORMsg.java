package bgu.spl171.net.impl.msg;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import bgu.spl171.net.impl.server.EncodeDecodeFunctions;
import bgu.spl171.net.impl.server.Number;
import bgu.spl171.net.impl.server.Packet;

public class ERRORMsg extends Message {
	
	private short errcode;
	private String errmsg;

	/**
	 * Constructor
	 * @param errorCode is Short
	 */
	public ERRORMsg(Short errorCode) {
		opcode = Packet.ERROR;
		errcode = errorCode;
		errmsg = geterrmsg(errorCode);
	}
	
	/**
	 * Decodes the ERROR message
	 */
	@Override
	public Message decodeNextByte(byte newByte)
	{
		Message msg = null;
        if(newByte != Number.ZERO) {
        	pushByte(newByte);
        }else{
        	errcode = encdec.bytesToShort(Arrays.copyOfRange(m_bytes, Number.ZERO, Number.TWO));
        	errmsg = encdec.decodeString(Arrays.copyOfRange(m_bytes, Number.TWO, m_bytes.length));
            msg = this;
        }
        return msg;
	}
	
	/**
	 * Encodes the ERROR message
	 */
	@Override
	public byte[] encode() {
		EncodeDecodeFunctions enc = new EncodeDecodeFunctions();		
		byte[] opcodebytes = enc.shortToBytes(opcode);
		byte[] errcodebytes = enc.shortToBytes(errcode);
		byte[] errmsgbytes = errmsg.getBytes(StandardCharsets.UTF_8);
		int i = Number.ZERO, len = opcodebytes.length+errcodebytes.length;
		byte[] bytes = new byte[errmsgbytes.length+len+1];
		bytes[Number.ZERO] = opcodebytes[Number.ZERO];
		bytes[Number.ONE] = opcodebytes[Number.ONE];
		bytes[Number.TWO] = errcodebytes[Number.ZERO];
		bytes[Number.THREE] = errcodebytes[Number.ONE];
		for(i = Number.ZERO; i < errmsgbytes.length ; i++)
		{
			bytes[len++] = errmsgbytes[i];
		}
		bytes[len] = Number.ZERO;
		return bytes;
	}
	
	public String toString()
	{
		return "ERRORMsg: errcode - " + errcode + "msg: " + errmsg;
	}

	/**
	 * @param errorCode is Short
	 * @return String - the error message according to the given errorCode
	 */
	private  String geterrmsg(Short errorCode)
	{
		switch (errorCode){
				
			case Number.ZERO:
				return "Not defined, see error message (if any)";
			case Number.ONE:
				return "File not found RRQ\\DELRQ of non-existing file";
			case Number.TWO:
				return "Access violation – File cannot be written, read or deleted.";
			case Number.THREE:
				return "Disk full or allocation exceeded – No room in disk.";
			case Number.FOUR:
				return "Illegal TFTP operation - Unknown Opcode.";
			case Number.FIVE:
				return  "File already exists – File name exists on WRQ.";
			case Number.SIX:
				return  "User not logged in – Any opcode received before Login completes.";
			case Number.SEVEN:
				return  "User already logged in – Login username already connected.";
		}
		return "wrong errorCode " + errorCode;		
	}
}
