package bgu.spl171.net.impl.msg;

import java.nio.charset.StandardCharsets;

import bgu.spl171.net.api.bidi.Connections;
import bgu.spl171.net.impl.server.FilesHandler;
import bgu.spl171.net.impl.server.Number;
import bgu.spl171.net.impl.server.Packet;
import bgu.spl171.net.srv.BaseServer;

public class DIRQMsg extends Message {

	/**
	 * Default constructor
	 */
	 public DIRQMsg()
    {    
    	opcode = Packet.DIRQ;
    	m_bytes = new byte[Number.MAX_LIMIT];
    }

	public Message execute(Object... objs)
	{	
		String str = "";
		Message response = null;
		if(!FilesHandler.files.isEmpty())
		{			
			for(String key: FilesHandler.files.keySet())
			{
				if(FilesHandler.files.get(key) != Number.TWO)
				{
					str += key;
					str += '\0';
				}
			}
		}
		else
		{
			str += "No files found";
			str += '\0';
		}
		response = new DATAMsg((short) Number.ONE, str.getBytes(StandardCharsets.UTF_8));
		return response;
	}
}
