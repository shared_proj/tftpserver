package bgu.spl171.net.impl.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.LinkedBlockingDeque;

import bgu.spl171.net.api.bidi.Connections;
import bgu.spl171.net.impl.msg.ACKMsg;
import bgu.spl171.net.impl.msg.BCASTMsg;
import bgu.spl171.net.impl.msg.ERRORMsg;
import bgu.spl171.net.impl.msg.Message;
import bgu.spl171.net.srv.BaseServer;

public class ReadDataManager extends Message {

	private LinkedBlockingDeque<Byte[]> file;
	private int blockNumber;
	private String filename;
	private byte [] m_bytes;
	private int len;
	private short oc= 0;
	private short packetSize=0;
	private boolean isCompleted;
	private boolean LastPacket;
	private boolean RWState;
	private EncodeDecodeFunctions endec =new EncodeDecodeFunctions();
	
	public ReadDataManager(){
//		this.file = new LinkedBlockingDeque<>();
//		this.blockNumber=0;
//		this.isCompleted=false;
//		this.LastPacket = false;
		this.RWState=false;
//		this.m_bytes=new byte[518];
//		this.len=0;
	}
	@Override
	public Message decodeNextByte(byte newByte){
		Message msg = null;
		m_bytes[len++]=newByte;
			if(len==2){
				this.packetSize = endec.bytesToShort(Arrays.copyOfRange(m_bytes, 0, len));
				if(this.packetSize<512)
				{
					this.LastPacket=true;
				}
				if(this.packetSize>512)
				{
					msg=new ERRORMsg((short)0);
				}
				System.out.println("ReadDataManager:packetsize:" + this.packetSize);
			}
		else if(len==4){
			short block = endec.bytesToShort(Arrays.copyOfRange(m_bytes,2, len));
			if(block !=this.blockNumber+1)
			{
				msg=new ERRORMsg((short)0);
			}
			System.out.println("ReadDataManager:blocknumber:" + block);
			
		}
		else if(len>4 &&this.packetSize<=512){
				this.packetSize--;
				if(this.packetSize==0)
				{
					file.push(toPrimitives(Arrays.copyOfRange(m_bytes,4, len)));
					msg = new ACKMsg((short)++this.blockNumber);
					if(LastPacket)
					{
						this.isCompleted=true;
						msg=this;
					}
					else resetSpecificVariables();
				}	
			}	
		return msg;
	}
	public void pushByte(byte nextByte) {
		  if (len >= m_bytes.length) {
			  m_bytes = Arrays.copyOf(m_bytes, len * 2);
		  }
		  m_bytes[len] = nextByte;
		}
	public boolean isComleted()
	{
		return this.isCompleted;
	}
	public void resetSpecificVariables()
	{
		m_bytes=new byte[518];
		len = 0;
		oc= 0;
		packetSize=0;
		}
	public void resetAll()
	{
		file = new LinkedBlockingDeque<Byte[]>();
		blockNumber=0;
		filename =null;
		m_bytes=new byte[518];
		len = 0;
		oc= 0;
		packetSize=0;
		isCompleted=false;
		LastPacket=false;
		SetRWState(true);
	}
	
	public boolean CreateFile()
	{
		try (FileOutputStream fileOuputStream = new FileOutputStream(FilesHandler.path + File.separator + this.filename);){ 
		    
		    for(Byte[] bytes : this.file)
		    	for(int i=0;i<bytes.length;i++)
		    		fileOuputStream.write(bytes[i]);
		    return true;
		    
		 } catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e){ 
			e.printStackTrace();
		}
		return false;
		 
	}
	Byte[] toPrimitives(byte[] oBytes)
	{
		Byte[] bytes = new Byte[oBytes.length];

	    for(int i = 0; i < oBytes.length; i++) {
	        bytes[i] = oBytes[i];
	    }
	    return bytes;
	}
	
	@Override
	public Message execute(Object... objs){
		Message msg =null;
		Connections<Message> cons = (Connections<Message>)objs[0];
		int connectionId = (int)objs[1];
		if(this.isComleted())
		{
//			if(getSumOfBytes()>BaseServer.getFreeSpace())
//			{
//				return new ERRORMsg((short)3);
//			}			
			    
			if(this.CreateFile()){
				System.out.println("File " + this.filename +" Created");
				FilesHandler.files.put(this.filename,0);
				msg = new BCASTMsg(this.filename,(byte) 1);
				cons.send(connectionId, new ACKMsg((short) this.blockNumber));
			}
			else{
				msg = new ERRORMsg((short)1);
			}
			SetRWState(false);
			return msg;
			
		}
		SetRWState(false);
		return new ERRORMsg((short)0); 
	}
	
	public void SetRWState(boolean state){
		this.RWState=state;
	}
	public boolean GetRWState()
	{
		return RWState;
	}
	public void SetFileName(String filename){
		this.filename=filename;
	}
	private long getSumOfBytes()
	{
		long sum=0;
		for(Byte[] bytes : this.file)
			sum+=bytes.length;
		return sum;
			
	}
	
}
