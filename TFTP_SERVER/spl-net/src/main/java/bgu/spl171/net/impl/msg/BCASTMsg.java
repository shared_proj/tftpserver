package bgu.spl171.net.impl.msg;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import bgu.spl171.net.api.bidi.Connections;
import bgu.spl171.net.impl.server.EncodeDecodeFunctions;
import bgu.spl171.net.impl.server.Number;
import bgu.spl171.net.impl.server.Packet;

public class BCASTMsg extends Message {

	private byte deloradd;
	private String filename;
	
	/**
	 * Default constructor
	 */
	public BCASTMsg()
	{
		m_bytes = new byte[Number.MAX_LIMIT];
		opcode = Packet.BCAST;
		deloradd = Number.ONE;
		filename = "";
	}
	
	/**
	 * Constructor
	 */
	public BCASTMsg(String file_name, byte delOradd)
	{
		m_bytes = new byte[Number.MAX_LIMIT];
		opcode = Packet.BCAST;
		deloradd = delOradd;
		filename = file_name;
	}
	
	/**
	 * Decodes the BCAST message
	 */
	@Override
	public Message decodeNextByte(byte newByte)
	{
		Message msg = null;
        if(newByte != Number.ZERO) {
        	pushByte(newByte);
        }else{
        	deloradd = m_bytes[Number.ZERO];
        	filename = encdec.decodeString(Arrays.copyOfRange(m_bytes, Number.ONE, m_bytes.length+Number.ONE));
            msg = this;
        }
        return msg;
	}
	
	/**
	 * Encodes the BCAST message
	 */
	@Override
	public byte[] encode() {
		byte[] opcodebytes = encdec.shortToBytes(opcode);
		byte[] filenamebytes = filename.getBytes(StandardCharsets.UTF_8);
		int i = Number.ZERO, len = opcodebytes.length+Number.ONE;
		byte[] bytes = new byte[len+filenamebytes.length+Number.ONE];		
		bytes[Number.ZERO]=opcodebytes[Number.ZERO];
		bytes[Number.ONE]=opcodebytes[Number.ONE];
		bytes[Number.TWO]=deloradd;
		for(i = Number.ZERO; i < filenamebytes.length ; i++)
		{
			bytes[len++] = filenamebytes[i];
		}
		bytes[len] = Number.ZERO;
		return bytes;
	}
	
	@Override
	public Message execute(Object... objs)
	{
		Connections<?> cons = (Connections<?>) objs[Number.ZERO];
		Message response;
		short opcode = (short) objs[Number.ONE];
		if(opcode == Packet.WRQ)
		{
			deloradd = Number.ONE;
			response = this;
		}
		else if(opcode == Packet.DELRQ)
		{
			deloradd = Number.ZERO;
			response = this;
		}
		else	
			response = new ERRORMsg(Number.ZERO);
		return response;
	}

}
