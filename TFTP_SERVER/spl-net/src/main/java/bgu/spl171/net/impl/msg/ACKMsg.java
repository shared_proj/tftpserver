package bgu.spl171.net.impl.msg;

import bgu.spl171.net.impl.server.EncodeDecodeFunctions;
import bgu.spl171.net.impl.server.Number;
import bgu.spl171.net.impl.server.Packet;

public class ACKMsg extends Message{

	private short block;

	/**
	 * Default constructor
	 */
	public ACKMsg()
	{
		m_bytes = new byte[Number.MAX_LIMIT];
		opcode = Packet.ACK;
		block = Number.ZERO;
	}
	
	/**
	 * Constructor
	 * @param blocknum is short
	 */
	public ACKMsg(short blocknum)
	{
		opcode = Packet.ACK;
		block = blocknum;
	}
	
	/**
	 * Decodes the ACK message
	 */
	@Override
	public Message decodeNextByte(byte newByte)
	{
		Message msg = null;
        if(newByte != Number.ZERO) {
        	pushByte(newByte);
        }else{
        	block = encdec.bytesToShort(m_bytes);
            msg = this;
        }
        return msg;
	}
	
	/**
	 * Encodes the ACK message
	 */
	@Override
	public byte[] encode() {
		EncodeDecodeFunctions enc = new EncodeDecodeFunctions();	
		byte[] opcodebytes = enc.shortToBytes(opcode);
		byte[] blockbytes = enc.shortToBytes(block);
		byte[] bytes = new byte[opcodebytes.length+blockbytes.length];
		bytes[Number.ZERO]=opcodebytes[Number.ZERO];
		bytes[Number.ONE]=opcodebytes[Number.ONE];
		bytes[Number.TWO]=blockbytes[Number.ZERO];
		bytes[Number.THREE]=blockbytes[Number.ONE];	
		return bytes;
	}

	@Override
	public String toString()
	{
		return "opcode: " + opcode + " block: " + block;
	}
}
