package bgu.spl171.net.impl.msg;

import bgu.spl171.net.api.bidi.Connections;
import bgu.spl171.net.impl.server.ConnectionsImpl;
import bgu.spl171.net.impl.server.EncodeDecodeFunctions;
import bgu.spl171.net.impl.server.LoggedInClient;
import bgu.spl171.net.impl.server.Number;
import bgu.spl171.net.impl.server.Packet;

public class LOGRQMsg extends Message {
    
	private String username = null;
   
	/**
	 * Default constructor
	 */
    public LOGRQMsg(){
        m_bytes = new byte[Number.MAX_LIMIT];
        opcode = Packet.LOGRQ;
    }
   
    /**
	 * Decodes the LOGRQ message
	 */
    @Override
    public Message decodeNextByte(byte newByte) {
        Message msg = null;
        if(newByte != Number.ZERO) {
        	pushByte(newByte);
        }else{
            username = encdec.decodeString(m_bytes);
            msg = this;
        }
        return msg;
    }
    
    /**
	 * Checks if the client is not logged in and if so - adds the client to
	 * logged_in_clients and sends ACK, otherwise sends ERROR
	 */
    @Override
    public Message execute(Object... objs) {
    	Connections<?> cons = (Connections<?>) objs[Number.ZERO];		
    	LoggedInClient is_logged_in = (LoggedInClient)objs[Number.ONE];
		Integer connid = (Integer) objs[Number.TWO];
		Message response;
		if(!((ConnectionsImpl<?>)cons).get_logged_in_clients().containsKey(username) && !is_logged_in.getis_logged_in())
		{
			is_logged_in.setis_logged_in(true);
			is_logged_in.setusername(username);
			((ConnectionsImpl<?>)cons).addusername(is_logged_in.getusername(),connid.intValue());
			response = new ACKMsg();
		}
		else if (!((ConnectionsImpl<?>)cons).get_logged_in_clients().containsKey(username) && is_logged_in.getis_logged_in())
		{
			response = new ERRORMsg(Number.ZERO);
		}
		else 
		{
			response = new ERRORMsg(Number.SEVEN);
		}
		return response;
	}
    
}