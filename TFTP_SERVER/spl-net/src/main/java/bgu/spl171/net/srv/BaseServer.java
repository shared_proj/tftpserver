package bgu.spl171.net.srv;

import bgu.spl171.net.api.MessageEncoderDecoder;
import bgu.spl171.net.api.bidi.BidiMessagingProtocol;
import bgu.spl171.net.api.bidi.Connections;
import bgu.spl171.net.impl.server.ConnectionsImpl;
import bgu.spl171.net.impl.server.FilesHandler;
import bgu.spl171.net.impl.server.Number;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public abstract class BaseServer<T> implements Server<T> {

    private final int port;
    private final Supplier<BidiMessagingProtocol<T>> protocolFactory;
    private final Supplier<MessageEncoderDecoder<T>> encdecFactory;
    private ServerSocket sock;
    private static FilesHandler fileshandler;
    private static AtomicInteger id = new AtomicInteger(Number.ZERO);
    
    public BaseServer(
            int port,
            Supplier<BidiMessagingProtocol<T>> protocolFactory,
            Supplier<MessageEncoderDecoder<T>> encdecFactory) {

        this.port = port;
        this.protocolFactory = protocolFactory;
        this.encdecFactory = encdecFactory;
		this.sock = null;
    }

    @Override
    public void serve() {
    	Connections<T> cons = new ConnectionsImpl<T>();
    	String path;
    	ConcurrentHashMap<String, Integer> files = new ConcurrentHashMap<String, Integer>();
    	path = System.getProperty("user.dir");
    	fileshandler = new FilesHandler(path, files);
    	fileshandler.updateServerFiles();
    	
        try (ServerSocket serverSock = new ServerSocket(port)) {

            this.sock = serverSock; //just to be able to close

            while (!Thread.currentThread().isInterrupted()) {

                Socket clientSock = serverSock.accept();               
                System.out.println("ACK " + clientSock.getInetAddress());
                BidiMessagingProtocol<T> prot = protocolFactory.get();               
                prot.start(id.get(), cons);
                
                BlockingConnectionHandler<T> handler = new BlockingConnectionHandler<T>(
                        clientSock,
                        encdecFactory.get(),
                        prot,
                        cons);
               
                ((ConnectionsImpl<T>)cons).addconnectedclient(id.getAndIncrement(), handler);

                execute(handler);              
                
            }
        } catch (IOException ex) { }

        System.out.println("server closed!!!");
    }

    @Override
    public void close() throws IOException {
		if (sock != null)
			sock.close();
    }
    protected abstract void execute(BlockingConnectionHandler<T>  handler);
    
}
