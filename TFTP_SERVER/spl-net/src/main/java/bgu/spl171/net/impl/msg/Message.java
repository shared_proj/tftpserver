package bgu.spl171.net.impl.msg;

import java.util.Arrays;

import bgu.spl171.net.impl.server.EncodeDecodeFunctions;
import bgu.spl171.net.impl.server.Number;

public  class Message {
	
	protected short opcode;
	protected byte[] m_bytes = null;   
	protected int len = Number.ZERO;
	protected EncodeDecodeFunctions encdec = new EncodeDecodeFunctions();
	
	/**
	 * The default decode function for the messages.
	 * Prints error.
	 * Each message can override this function according to it's needs.
	 * @param nextByte
	 * @return null
	 */
	public Message decodeNextByte(byte nextByte)
	{
		System.out.println("You cannot decode this msg " +  opcode + "! ");
		return null;
	}
	
	/**
	 * The default encode function for the messages.
	 * Prints error.
	 * Each message can override this function according to it's needs.
	 * @return null
	 */
	public byte[] encode()
	{
		System.out.println("You cannot encode this  msg " +  opcode + "! ");
		return null;
	}
	
	/**
	 * The default execute function for the messages.
	 * Prints error.
	 * Each message can override this function according to it's needs.
	 * @param objs
	 * @return null
	 */
	public Message execute(Object... objs)
	{
		System.out.println("You cannot execute this msg " +  opcode + "! ");
		return null;
	}
	
	/**
	 * @return opcode is short
	 */
	public short getOpcode(){
		return opcode;
	}

	/**
	 * Push the byte to m_bytes
	 * @param nextByte is byte
	 */
	public void pushByte(byte nextByte) {
	  if (len >= m_bytes.length) {
		  m_bytes = Arrays.copyOf(m_bytes, len * Number.TWO);
	  }
	  m_bytes[len++] = nextByte;
	}
}
