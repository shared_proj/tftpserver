package bgu.spl171.net.impl.server;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import bgu.spl171.net.api.MessageEncoderDecoder;
import bgu.spl171.net.impl.msg.ACKMsg;
import bgu.spl171.net.impl.msg.BCASTMsg;
import bgu.spl171.net.impl.msg.DATAMsg;
import bgu.spl171.net.impl.msg.DELRQMsg;
import bgu.spl171.net.impl.msg.DIRQMsg;
import bgu.spl171.net.impl.msg.DISCMsg;
import bgu.spl171.net.impl.msg.ERRORMsg;
import bgu.spl171.net.impl.msg.LOGRQMsg;
import bgu.spl171.net.impl.msg.Message;
import bgu.spl171.net.impl.msg.RRQMsg;
import bgu.spl171.net.impl.msg.WRQMsg;

public class MessageEncoderDecoderImpl implements MessageEncoderDecoder<Message>{

    private byte[] bytes = new byte[Number.TWO];
    private int len = Number.ZERO;
    private Message build_msg = null;
    private Message msg = null;
	private EncodeDecodeFunctions encdec = new EncodeDecodeFunctions();
	private boolean RWState=false;
	private ReadDataManager readManager = new ReadDataManager();	
	private Message rrqMsg;
	
	

    /**
     * Calls the decodeNextByte function of each message according to the opcode 
     */
    @Override
    public Message decodeNextByte(byte newByte) {     
        if(len <= Number.ONE ){
        	msg = null;
            bytes[len++] = newByte;
            if( Number.TWO == len ){
            	build_msg = MessageFactory(encdec.bytesToShort(bytes));
            	if (build_msg.getOpcode() == Packet.DISC || build_msg.getOpcode() == Packet.DIRQ){
            		len = Number.ZERO;
            		return build_msg;
            	}
            }
        }else{
            msg = build_msg.decodeNextByte(newByte);
            
            if (null != msg){
            	len = Number.ZERO;
            	bytes[Number.ONE]=Number.ZERO;
            }
        }
        if(readManager.GetRWState()==false)
        {
        	this.RWState=false;
        }
        return msg;
    }

    /**
     * Calls the encode function of each message 
     */
	@Override
	public byte[] encode(Message message) {
		return message.encode();
		}
	
	/**
	 * @param op is short
	 * @return Message according to the given opcode
	 */
	Message MessageFactory(short op){
		Message message  = null;
		switch (op){
			case Packet.RRQ:
				message = new RRQMsg();
				this.rrqMsg = message;
				//this.RWState = true;
				break;
			case Packet.WRQ:
				message = new WRQMsg(this.readManager);
				RWState=true;
				readManager.resetAll();
				//this.RWState = true;
				break;
			case Packet.DATA:
				if(!this.readManager.GetRWState())
					message = new ERRORMsg((short)2);
				else
				{
					message = this.readManager;
				}
				//message = new DATAMsg();
				break;
			case Packet.ACK:
				if(((RRQMsg)this.rrqMsg).getState())
				{
					//msg= this.sendManager;
					message = this.rrqMsg;
				}
				else message = new ACKMsg();
				break;
			case Packet.ERROR:
				//message = new ERRORMsg();
				//can client send the server an error?
				break;
			case Packet.DIRQ:
				message = new DIRQMsg();
				break;
			case Packet.LOGRQ:
				message = new LOGRQMsg();		
				break;
			case Packet.DELRQ:
				message = new DELRQMsg();
				break;
			case Packet.DISC:
				message = new DISCMsg();
				break;	
			default: 
				System.out.println("Wrong opcode " + op);
				break;
		}
		return message;
	} 
	
}