package bgu.spl171.net.srv;

import bgu.spl171.net.api.MessageEncoderDecoder;
import bgu.spl171.net.api.bidi.BidiMessagingProtocol;
import bgu.spl171.net.api.bidi.Connections;
import bgu.spl171.net.impl.server.BidiMessagingProtocolImpl;
import bgu.spl171.net.impl.server.ConnectionsImpl;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ConcurrentSkipListSet;

public class BlockingConnectionHandler<T> implements Runnable, ConnectionHandler<T> {

    private final BidiMessagingProtocol<T> protocol;
    private final MessageEncoderDecoder<T> encdec;
    private final Socket sock;
    private BufferedInputStream in;
    private BufferedOutputStream out;
    private volatile boolean connected = true;   
    private Connections<T> cons;
    private ConcurrentSkipListSet<String> files;
    
    public BlockingConnectionHandler(Socket sock, MessageEncoderDecoder<T> reader, BidiMessagingProtocol<T> protocol, Connections<T> cons) {
        this.sock = sock;
        this.encdec = reader;
        this.protocol = protocol;
        this.cons = cons;
    }

    @Override
    public void run() {

        try (Socket sock = this.sock) { //just for automatic closing
            int read;
            in = new BufferedInputStream(sock.getInputStream());
            out = new BufferedOutputStream(sock.getOutputStream());
            while (!protocol.shouldTerminate() && connected && (read = in.read()) >= 0) {
            	System.out.println(read);
            	T nextMessage = encdec.decodeNextByte((byte) read);
                if (nextMessage != null) {     
                	protocol.process(nextMessage);                  
                }
            }
            ((ConnectionsImpl<T>)cons).disconnect(((BidiMessagingProtocolImpl)protocol).getconnId());
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void close() throws IOException {
        connected = false;
        sock.close();
    }

	@Override
	public void send(T msg) {
		if (msg != null) {
            try {
				out.write(encdec.encode(msg));
				out.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}            
        }		
	}
}
