package bgu.spl171.net.impl.server;

/**
 * Enum for numbers
 */
public class Number {
	
	public static final short ZERO = 0;
	public static final short ONE = 1;
	public static final short TWO = 2;
	public static final short THREE = 3;
	public static final short FOUR = 4;
	public static final short FIVE = 5;
	public static final short SIX = 6;
	public static final short SEVEN = 7;
	public static final short EIGHT = 8;
	public static final short NINE = 9;
	public static final short TEN = 10;
	public static final short MAX_LIMIT = 600;
	public static final short PACKET_SIZE = 512;
	public static final short PORT = 7777;
	
}
