package bgu.spl171.net.impl.msg;

import bgu.spl171.net.api.bidi.Connections;
import bgu.spl171.net.impl.server.ConnectionsImpl;
import bgu.spl171.net.impl.server.LoggedInClient;
import bgu.spl171.net.impl.server.Number;
import bgu.spl171.net.impl.server.Packet;

public class DISCMsg extends Message {

	/**
	 * Default constructor
	 */
	public DISCMsg()
	{
		opcode = Packet.DISC;
	}
	
	/**
	 * Checks if the client is logged in and if so - removes the client from
	 * logged_in_clients and sends ACK, otherwise sends ERROR
	 */
	@Override
	public Message execute(Object...objs) {
		Connections<?> cons = (Connections<?>) objs[Number.ZERO];
		LoggedInClient is_logged_in = (LoggedInClient)objs[Number.ONE];
		if(((ConnectionsImpl<?>)cons).get_logged_in_clients().containsKey(is_logged_in.getusername())){
			((ConnectionsImpl<?>)cons).removeusername(is_logged_in.getusername());
			return new ACKMsg();
		}
		return new ERRORMsg((short) Number.SIX);
	}

}
