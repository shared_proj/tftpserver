package bgu.spl171.net.impl.msg;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import bgu.spl171.net.api.bidi.Connections;
import bgu.spl171.net.impl.server.EncodeDecodeFunctions;
import bgu.spl171.net.impl.server.FilesHandler;
import bgu.spl171.net.impl.server.LoggedInClient;
import bgu.spl171.net.impl.server.Number;
import bgu.spl171.net.impl.server.Packet;
import bgu.spl171.net.srv.BaseServer;

public class RRQMsg extends Message {

	private String filename;
	private boolean sendState;
	private int len=0;
	private short packetSize=0;
	private boolean isCompleted=false;
	private boolean LastPacket=false;
	private boolean RWState=false;
	private int blockNumber=0;
	private long fileSize;
	private RandomAccessFile f;
	/**
	 * Constructor
	 * @param filname is String
	 */
	public RRQMsg()
	{
		opcode = Packet.RRQ;
		this.filename = null;
		m_bytes = new byte[518];
		this.sendState=false;
	}
		
	@Override
    public Message decodeNextByte(byte newByte) {
        Message msg = null;
        if (!this.sendState){
	        if(newByte != 0) {
	            m_bytes[len++] = newByte;
	            System.out.println((char)newByte);
	        }else{
	        	filename = encdec.decodeString(m_bytes);
	            msg = this;
	        }
	        return msg;
		}
	    else
	    {
	    	
	    	m_bytes[len++]=newByte;
			if(len==2){
				short tempblock = encdec.bytesToShort(Arrays.copyOfRange(m_bytes, 0, len));
				if(tempblock==this.blockNumber)
				{
					if (!isCompleted)
						return this;
				}
				else
					return new ERRORMsg((short)1);				
			}
			return msg;
	    }
    }
	@Override
	public Message execute(Object... objs)
	{
		Message msg = null;
		Connections<Message> cons = (Connections<Message>)objs[0];
		LoggedInClient is_logged_in = (LoggedInClient)objs[1];
        Integer connid = (Integer) objs[2];
        		
		if (!this.sendState){
			for (String str : FilesHandler.files.keySet())
			{
				if(this.filename.equals(str)){
					if(FilesHandler.files.get(str)==2){
						return new ERRORMsg((short)0);
					}
				}
			}	
			this.sendState=true;
			msg = new ACKMsg();
			cons.send(connid.intValue(), new ACKMsg());
			try {
				this.f = new RandomAccessFile(FilesHandler.path+ File.separator + filename,"r");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			this.fileSize = new File(FilesHandler.path+ File.separator + filename).length();
		}		
		byte[] buffer;
		try { 
			if(this.fileSize>512){
				buffer = new byte[512];
				f.read(buffer,0, 512);
				this.fileSize-=512;
			}
			else 
			{
				buffer = new byte[(int)fileSize];
				f.read(buffer,0,((int)fileSize));
				this.isCompleted=true;
				f.close();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				f.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return new ERRORMsg((short)1);
		} 
		len=0;
		return new DATAMsg((short)++this.blockNumber,buffer);		
	}
	byte[] toPrimitives(Byte[] oBytes)
	{
		byte[] bytes = new byte[oBytes.length];

	    for(int i = 0; i < oBytes.length; i++) {
	        bytes[i] = oBytes[i];
	    }
	    return bytes;
	}
	public boolean getState()
	{
		return this.sendState;
	}
}
