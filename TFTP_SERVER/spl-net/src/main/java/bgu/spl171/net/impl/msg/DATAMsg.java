package bgu.spl171.net.impl.msg;

import java.util.Arrays;

import bgu.spl171.net.impl.server.EncodeDecodeFunctions;
import bgu.spl171.net.impl.server.Number;
import bgu.spl171.net.impl.server.Packet;


public class DATAMsg extends Message {

	private short opcode;
	private short packetsize;
	private short block;
	private byte[] data;
	
	/**
	 * Default constructor
	 */
	public DATAMsg()
	{
		m_bytes = new byte[Number.MAX_LIMIT];
		opcode = Packet.DATA;
		packetsize = Number.PACKET_SIZE;
		block = Number.ZERO;
		data = new byte[packetsize];
	}
	
	/**
	 * Constructor
	 */
	public DATAMsg(short block, byte[] data)
	{
		opcode = Packet.DATA;
		packetsize = (short) data.length;
		this.block = block;
		this.data = data;
	}
	
	/**
	 * Decodes the DATA message
	 */
	@Override
	public Message decodeNextByte(byte newByte)
	{
		Message msg = null;
        if(newByte != Number.ZERO) {
        	pushByte(newByte);
        }
        else
        {
        	packetsize = encdec.bytesToShort(Arrays.copyOfRange(m_bytes, Number.ZERO, Number.TWO));
        	block = encdec.bytesToShort(Arrays.copyOfRange(m_bytes, Number.TWO, Number.FOUR));
        	for(int i = Number.ZERO; i < m_bytes.length-Number.FOUR ;i++)
        	{
        		data[i] = m_bytes[i+Number.FOUR];
        	}
            msg = this;
        }
        return msg;
	}
	
	/**
	 * Encodes the DATA message
	 */
	@Override
	public byte[] encode() {				
		byte[] shortbytes = encdec.shortToBytes(opcode);
		byte[] packetsizebytes = encdec.shortToBytes(packetsize);
		byte[] blockbytes = encdec.shortToBytes(block);
		int i = Number.ZERO, len = shortbytes.length+packetsizebytes.length+blockbytes.length;
		byte[] bytes = new byte[data.length+len];
		bytes[Number.ZERO] = shortbytes[Number.ZERO];
		bytes[Number.ONE] = shortbytes[Number.ONE];
		bytes[Number.TWO] = packetsizebytes[Number.ZERO];
		bytes[Number.THREE] = packetsizebytes[Number.ONE];
		bytes[Number.FOUR] = blockbytes[Number.ZERO];
		bytes[Number.FIVE] = blockbytes[Number.ONE];
		for(i = Number.ZERO; i < data.length ; i++)
		{
			bytes[len++] = data[i];
		}
		return bytes;
	}

	@Override
	public Message execute(Object... objs)
	{
		return new ACKMsg(block);
	}
	
}
