package bgu.spl171.net.impl.server;

import java.nio.charset.StandardCharsets;

/**
 * This class contains all the encode/decode functions
 */
public class EncodeDecodeFunctions {

  public byte[] shortToBytes(short num)
    {
        byte[] bytesArr = new byte[Number.TWO];
        bytesArr[Number.ZERO] = (byte)((num >> Number.EIGHT) & 0xFF);
        bytesArr[Number.ONE] = (byte)(num & 0xFF);
        return bytesArr;
    }
  
	public String decodeString(byte[] bytes) {
		int i;
		for(i=Number.ZERO; bytes[i]!=Number.ZERO; i++);
		String username = new String( bytes, 0, i, StandardCharsets.UTF_8);
        return username;
	}
	
    public short bytesToShort(byte[] byteArr)
    {
        short result = (short)((byteArr[Number.ZERO] & 0xff) << Number.EIGHT);
        result += (short)(byteArr[Number.ONE] & 0xff);
        return result;
    }
  
}
